\id JAS
\h Jakobs
\toc1 Jakobs' brev.
\toc2 Jakobs.
\toc3 Jak
\c 1
\s Jakob hälsar de kristna av Israel, som
\s bo kringspridda bland folken; förmanar
\s dem till ståndaktighet i frestelser och
\s till en gudsdyrkan som visar sig verksam
\s i goda gärningar.
\p
\v 1 Jakob, Guds och Herrens, Jesu Kristi, tjänare, hälsar de tolv stammar som bo kringspridda bland folken.
\x + Joh 7,35\x*
\p
\v 2 Mina bröder, hållen det för idel glädje, när I kommen i allahanda frestelser,
\x + 1 Petr 1,6\x*
\v 3 och veten, att om eder tro håller provet, så verkar detta ståndaktighet.
\x + Rom 5,3\x*
\v 4 Och låten ståndaktigheten hava med sig fullkomlighet i gärning, så att I ären fullkomliga, utan fel och utan brist i något stycke.
\p
\v 5 Men om någon av eder brister i vishet, så må han utbedja sig sådan från Gud, som giver åt alla villigt och utan hårda ord, och den skall bliva honom given.
\x + 1 Kon 3,5; Ords 2,3; Jer 29,12; Vish 8,21\x*
\x + Matt 7,7; Jak 5,13\x*
\v 6 Men han bedje i tro, utan att tvivla; ty den som tvivlar är lik havets våg, som drives omkring av vinden och kastas hit och dit.
\x + Matt 21,21; Mark 11,24; Joh 14,13\x*
\v 7 En sådan människa må icke tänka att hon skall få något från Herren --
\v 8 en människa med delad håg, en som går ostadigt fram på alla sina vägar.
\x + Jak 4,8\x*
\p
\v 9 Den broder som lever i ringhet berömme sig av sin höghet.
\x + Jak 2,5\x*
\v 10 Den åter som är rik berömme sig av sin ringhet, ty såsom gräsets blomster skall han förgås.
\x + Ps 103,15; Jes 40,6; 2 Kor 12,9; 1 Petr 1,24\x*
\v 11 Solen går upp med sin brännande hetta och förtorkar gräset, och dess blomster faller av, och dess fägring förgår; så skall ock den rike förvissna mitt i sin ävlan.
\x + 1 Joh 2,17\x*
\p
\v 12 Salig är den man som är ståndaktig i frestelsen; ty när han har bestått sitt prov, skall han få livets krona, vilken Gud har lovat åt dem som älska honom.
\x + Matt 10,22; 2 Tim 4,7; 1 Petr 3,14; Upp 2,10\x*
\p
\v 13 Ingen säge, när han bliver frestad, att det är från Gud som hans frestelse kommer; ty såsom Gud icke kan frestas av något ont, så frestar han icke heller någon.
\x + Sir 15,11\x*
\v 14 Nej, närhelst någon frestas, så är det av sin egen begärelse som han drages och lockas.
\x + Rom 7,7\x*
\v 15 Sedan, när begärelsen har blivit havande, föder hon synd, och när synden har blivit fullmogen, framföder hon död.
\x + Rom 6,23\x*
\v 16 Faren icke vilse, mina älskade bröder.
\v 17 Idel goda gåvor och idel fullkomliga skänker komma ned ovanifrån, från himlaljusens Fader, hos vilken ingen förändring äger rum och ingen växling av ljus och mörker.
\x + Joh 3,27; 1 Kor 4,7; 1 Joh 1,5\x*
\v 18 Efter sitt eget beslut födde han oss till liv genom sanningens ord, för att vi skulle vara en förstling av de varelser han har skapat.
\x + Joh 1,13; 1 Kor 4,15; 1 Petr 1,23; Upp 14,4\x*
\v 19 Det veten I, mina älskade bröder.
\p Men var människa vare snar till att höra och sen till att tala och sen till vrede.
\x + Ords 17,27; Sir 5,11; Jak 3,1,13\x*
\v 20 Ty en mans vrede kommer icke åstad vad rätt är inför Gud.
\v 21 Skaffen därför bort all orenhet och all ondska som finnes kvar, och mottagen med saktmod det ord som är plantat i eder, och som kan frälsa edra själar.
\x + Luk 8,11.15; Rom 1,16.13,12; Kol 3,8; 1 Petr 2,1\x*
\v 22 Men varen ordets görare, och icke allenast dess hörare, eljest bedragen I eder själva.
\x + Matt 7,21.24; Luk 11,28; Rom 2,13; 1 Joh 3,7\x*
\v 23 Ty om någon är ordets hörare, men icke dess görare, så är han lik en man som betraktar sitt ansikte i en spegel:
\x + Matt 7,26\x*
\v 24 när han har betraktat sig däri, går han sin väg och förgäter strax hurudan han var.
\v 25 Men den som skådar in i den fullkomliga lagen, frihetens lag, och förbliver därvid och icke är en glömsk hörare, utan en verklig görare, han varder salig i sin gärning.
\x + Joh 8,31.13,17; Rom 8,2; Jak 2,12\x*
\v 26 Om någon menar sig tjäna Gud och icke tyglar sin tunga, utan bedrager sitt hjärta, så är hans gudstjänst intet värd.
\x + Ps 39,2; 1 Petr 3,10\x*
\v 27 En gudstjänst, som är ren och obesmittad inför Gud och Fadern, är det att vårda sig om fader- och moderlösa barn och änkor i deras bedrövelse, och att hålla sig obefläckad av världen.
\x + Job 31,16; Jes 58,6; Matt 25,35; Joh 17,15; Rom 12,2\x*
\c 2
\s Uppmaning att icke hava anseende till
\p
\s personen. Varning för en tro som icke
\s har med sig gärningar.
\p
\v 1 Mina bröder, menen icke att tron på vår Herre Jesus Kristus, den förhärligade, kan stå tillsammans med att hava anseende till personen.
\x + 3 Mos 19,15; Ords 24,23\x*
\v 2 Om till exempel i eder församling inträder en man med guldring på fingret och i präktiga kläder, och jämte honom inträder en fattig man i smutsiga kläder,
\v 3 och I då vänden edra blickar till den som bär de präktiga kläderna och sägen till honom: »Sitt du här på denna goda plats», men däremot sägen till den fattige: »Stå du där», eller: »Sätt dig därnere vid min fotapall» --
\v 4 haven I icke då kommit i strid med eder själva och blivit domare som döma efter orätta grunder?
\p
\v 5 Hören, mina älskade bröder: Har icke Gud utvald just dem som i världens ögon äro fattiga till att bliva rika i tro, och att få till arvedel det rike han har lovat åt dem som älska honom?
\x + Luk 6,20; 1 Kor 1,26.2,9\x*
\v 6 I åter haven visat förakt för den fattige. Är det då icke de rika som förtrycka eder, och är det icke just de, som draga eder inför domstolarna?
\x + 1 Kor 11,22\x*
\v 7 Är det icke de, som smäda det goda namn som är nämnt över eder?
\v 8 Om I, såsom skriften bjuder, fullgören den konungsliga lagen: »Du skall älska din nästa såsom dig själv», då gören I visserligen väl.
\x + 3 Mos 19,18\x*
\v 9 Men om I haven anseende till personen, så begån I synd och bliven av lagen överbevisade om att vara överträdare.
\x + 5 Mos 1,17.16,19\x*
\v 10 Ty om någon håller hela lagen i övrigt, men felar i ett, så är han skyldig till allt.
\x + 5 Mos 27,26; Matt 5,19\x*
\v 11 Densamme som sade: »Du skall icke begå äktenskapsbrott», han sade ju ock: »Du skall icke dräpa.» Om du nu visserligen icke begår äktenskapsbrott, men dräper, så är du dock en lagöverträdare.
\x + 2 Mos 20,13\x*
\v 12 Talen och handlen så, som det höves människor vilka skola dömas genom frihetens lag.
\x + Jak 1,25\x*
\v 13 Ty domen skall utan barmhärtighet drabba den som icke har visat barmhärtighet; barmhärtighet åter kan frimodigt träda fram inför domen.
\x + Matt 5,7.6,15. 18,32.25,31\x*
\p
\v 14 Mina bröder, vartill gagnar det, om någon säger sig hava tro, men icke har gärningar? Icke kan väl tron frälsa honom?
\x + Matt 7,21.21,30; Rom 6,1; Gal 5,6\x*
\v 15 Om någon, vare sig en broder eller en syster, saknade kläder och vore utan mat för dagen
\x + Luk 3,11\x*
\v 16 och någon av eder då sade till denne: »Gå i frid, kläd dig varmt, och ät dig mätt» -- vartill gagnade detta, såframt han icke därjämte gåve honom vad hans kropp behövde?
\x + 1 Joh 3,17\x*
\v 17 Så är ock tron i sig själv död, om den icke har med sig gärningar.
\p
\v 18 Nu torde någon säga: »Du har ju tro?» -- »Ja, och jag har också gärningar; visa mig du din tro utan gärningar, så vill jag genom mina gärningar visa dig min tro.»
\v 19 Du tror att Gud är en. Däri gör du rätt; också de onda andarna tro det och bäva.
\x + Mark 1,24\x*
\v 20 Men vill du då förstå, du fåkunniga människa, att tron utan gärningar är till intet gagn!
\v 21 Blev icke Abraham, vår fader, rättfärdig av gärningar, när han frambar sin son Isak på altaret?
\x + 1 Mos 22,1; Hebr 11,17\x*
\v 22 Du ser alltså att tron samverkade med hans gärningar, och av gärningarna blev tron fullkomnad,
\v 23 och så fullbordades det skriftens ord som säger: »Abraham trodde Gud, och det räknades honom till rättfärdighet»; och han blev kallad »Guds vän».
\x + 1 Mos 15,6; 2 Krön 20,7; Jes 41,8; Rom 4,3\x*
\p
\v 24 I sen alltså att det är av gärningar som en människa bliver rättfärdig, och icke av tro allenast.
\x + Joh 8,39\x*
\v 25 Och var det icke på samma sätt med skökan Rahab? Blev icke hon rättfärdig av gärningar, när hon tog emot sändebuden och sedan på en annan väg släppte ut dem?
\x + Jos 2,1.6,17.23\x*
\v 26 Ja, såsom kroppen utan ande är död, så är ock tron utan gärningar död.
\c 3
\s Tungans missbruk. Den sanna visheten.
\p
\p
\v 1 Mina bröder, icke många av eder må träda upp såsom lärare; I bören veta att vi skola få en dess strängare dom.
\x + Matt 12,36; Jak 1,19\x*
\v 2 I många stycken fela vi ju alla; om någon icke felar i sitt tal, så är denne en fullkomlig man, som förmår tygla hela sin kropp.
\x + Ords 20,9; Pred 7,21; Sir 19,16; 1 Petr 3,10; 1 Joh 1,8\x*
\x + Jak 1,26\x*
\v 3 När vi lägga betsel i hästarnas mun, för att de skola lyda oss, då kunna vi därmed styra också hela deras övriga kropp.
\v 4 Ja, till och med skeppen, som äro så stora, och som drivas av starka vindar, styras av ett helt litet roder åt det håll dit styrmannen vill.
\v 5 Så är ock tungan en liten lem och kan likväl berömma sig av stora ting. Betänken huru en liten eld kan antända en stor skog.
\x + Ps 12,4.140,2; Ords 12,18.15,2\x*
\v 6 Också tungan är en eld; såsom en värld av orättfärdighet framstår den bland våra lemmar, tungan som befläckar hela kroppen och sätter »tillvarons hjul» i brand, likasom den själv är antänd av Gehenna.
\x + Matt 15,11.18\x*
\v 7 Ty väl är det så, att alla varelsers natur, både fyrfotadjurs och fåglars och kräldjurs och vattendjurs, låter tämja sig, och verkligen har blivit tamd, genom människors natur.
\v 8 Men tungan kan ingen människa tämja; ett oroligt och ont ting är den, och full av dödande gift.
\v 9 Med den välsigna vi Herren och Fadern, och med den förbanna vi människorna, som äro skapade till att vara Gud lika.
\x + 1 Mos 1,26\x*
\v 10 Ja, från en och samma mun utgå välsignelse och förbannelse. Så bör det icke vara, mina bröder.
\v 11 Icke giver väl en källa från en och samma åder både sött och bittert vatten?
\v 12 Mina bröder, icke kan väl ett fikonträd bära oliver eller ett vinträd fikon? Lika litet kan en salt källa giva sött vatten.
\x + Matt 7,16\x*
\p
\v 13 Finnes bland eder någon vis och förståndig man, så må han, i visligt saktmod, genom sin goda vandel låta se de gärningar som hövas en sådan man.
\x + Ef 5,15; 1 Petr 2,12\x*
\v 14 Om I åter i edra hjärtan hysen bitter avund och ären genstridiga, då mån I icke förhäva eder och ljuga, i strid mot sanningen.
\x + Ef 4,31; 1 Joh 4,20\x*
\v 15 Sådan »vishet» kommer icke ned ovanifrån, utan är av jorden och tillhör de »själiska»[1] människorna, ja, de onda andarna.
\x + 1 Kor 2,6; Jak 1,17\x*
\v 16 Ty där avund och genstridighet råda, där råder oordning och allt vad ont är.
\x + 1 Kor 3,3; Gal 5,19\x*
\v 17 Men den vishet som kommer ovanifrån är först och främst ren, vidare fridsam, foglig och mild, full av barmhärtighet och andra goda frukter, fri ifrån tvivel, fri ifrån skrymtan.
\x + Vish 7,22\x*
\v 18 Och rättfärdighetens frukt kommer av en sådd i frid, dem till del som hålla frid.
\x + Jes 32,17\x*
\p [1] Se Själisk i Ordförkl.
\c 4
\s Varning för osämja, onda lustar,
\p
\s värdsligt sinne, högfärd, förtal,
\s stortalig förmätenhet.
\p
\v 1 Varav uppkomma strider, och varav tvister bland eder? Månne icke av de lustar som föra krig i eder lemmar?
\x + Rom 7,23; 1 Petr 2,11\x*
\v 2 I ären fulla av begärelser, men haven dock intet; I dräpen och hysen avund, men kunnen dock intet vinna; och så tvisten och striden I. I haven intet, därför att I icke bedjen.
\x + Ps 66,18\x*
\v 3 Dock, I bedjen, men I fån intet, ty I bedjen illa, nämligen för att kunna i edra lustar förslösa vad I fån.
\x + Job 27,8.35,12; Ords 1,28; Jes 1,15; Joh 15,7\x*
\x + 1 Joh 5,14\x*
\v 4 I trolösa avfällingar, veten I då icke att världens vänskap är Guds ovänskap? Den som vill vara världens vän, han bliver alltså Guds ovän.
\x + Matt 6,24; Joh 15,19.17,14; 2 Kor 11,2; 1 Joh 2,15\x*
\v 5 Eller menen I att detta är ett tomt ord i skriften: »Med svartsjuk kärlek trängtar den Ande som han har låtit taga sin boning i oss»?
\x + 2 Mos 34,14\x*
\v 6 Men så mycket större är den nåd han giver; därför heter det: »Gud står emot de högmodiga, men de ödmjuka giver han nåd.»
\x + Job 22,29; Ords 3,34.29,23; Sir 3,18; 1 Petr 5,5\x*
\v 7 Så varen nu Gud underdåniga, men stån emot djävulen, så skall han fly bort ifrån eder.
\x + Ef 4,27.6,11; 1 Petr 5,8; 1 Joh 5,18\x*
\v 8 Nalkens Gud, så skall han nalkas eder. Renen edra händer, I syndare, och gören edra hjärtan rena, I människor med delad håg.
\x + Jes 1,18; Sak 1,3\x*
\v 9 Kännen edert elände och sörjen och gråten. Edert löje vände sig i sorg och eder glädje i bedrövelse.
\x + Luk 6,25\x*
\v 10 Ödmjuken eder inför Herren, så skall han upphöja eder.
\x + Luk 14,11\x*
\p
\v 11 Förtalen icke varandra, mina bröder. Den som förtalar en broder eller dömer sin broder, han förtalar lagen och dömer lagen. Men dömer du lagen, så är du icke en lagens görare, utan dess domare.
\x + 3 Mos 19,16; Ps 15,3; Matt 7,1; Rom 2,1.14,4; 1 Kor 4,5\x*
\x + 1 Petr 2,1\x*
\v 12 En är lagstiftaren och domaren, han som kan frälsa och kan förgöra. Vem är då du som dömer din nästa?
\p
\v 13 Hören nu, I som sägen: »I dag eller i morgon vilja vi begiva oss till den och den staden, och där vilja vi uppehålla oss ett år och driva handel och skaffa oss vinning» --
\x + Ords 27,1; Luk 12,16\x*
\v 14 I veten ju icke vad som kan ske i morgon. Ty vad är edert liv? En rök ären I, som synes en liten stund, men sedan försvinner.
\x + Ps 39,6; Jes 40,6\x*
\v 15 I borden fastmera säga: »Om Herren vill, och vi får leva, skola vi göra det, eller det.»
\x + Apg 18,21\x*
\v 16 Men nu talen I stora ord i eder förmätenhet. All sådan stortalighet är ond.
\x + 1 Kor 5,6\x*
\p
\v 17 Alltså, den som förstår att göra vad gott är, men icke gör det, för honom bliver detta till synd.
\x + Luk 12,47; Rom 14,23\x*
\c 5
\s Dom över orättfärdigt rika. Förmaning
\p
\s till tålamod. Varning mot svärjande.
\s Uppmaning till bön och förbön. Plikten
\s att rädda den vilsefarande.
\p
\v 1 Hören nu, I rike: Gråten och jämren eder över det elände som skall komma över eder.
\x + Luk 6,24; 1 Tim 6,9\x*
\v 2 Eder rikedom multnar bort, och edra kläder frätas sönder av mal;
\x + Matt 6,19\x*
\v 3 edert guld och silver förrostar, och rosten därpå skall vara eder till ett vittnesbörd och skall såsom en eld förtära edert kött. I haven samlat eder skatter i de yttersta dagarna.
\v 4 Se, den lön I haven förhållit arbetarna som hava avbärgat edra åkrar, den ropar över eder, och skördemännens rop hava kommit fram till Herren Sebaots öron.
\x + 1 Mos 18,20; 3 Mos 19,13; 5 Mos 24,14; Job 31,38\x*
\x + Mal 3,5\x*
\v 5 I haven levat i kräslighet på jorden och gjort eder goda dagar; I haven gött eder av hjärtans lust »på eder slaktedag».
\x + Job 21,12; Jer 25,34; Luk 12,16.16,19\x*
\v 6 I haven dömt den rättfärdige skyldig och haven dräpt honom; han står eder icke emot.
\x + Matt 5,39\x*
\v 7 Så biden nu tåligt, mina bröder, intill Herrens tillkommelse. I sen huru åkermannen väntar på jordens dyrbara frukt och tåligt bidar efter den, till dess att den har fått höstregn och vårregn.
\x + 5 Mos 11,14; Sak 10,1; Hebr 10,36\x*
\v 8 Ja, biden ock I tåligt, och styrken edra hjärtan; ty Herrens tillkommelse är nära.
\x + 1 Kor 10,11\x*
\v 9 Sucken icke mot varandra, mina bröder, på det att I icke mån bliva dömda. Se, domaren står för dörren.
\v 10 Mina bröder, tagen profeterna, som talade i Herrens namn, till edert föredöme i att uthärda lidande och visa tålamod.
\x + Matt 5,12\x*
\v 11 Vi prisa ju dem saliga, som hava varit ståndaktiga. Om Jobs ståndaktighet haven I hört, och I haven sett vilken utgång Herren beredde; ty Herren är nåderik och barmhärtig.
\x + Job 1,20.42,10.12; Ps 103,8\x*
\p
\v 12 Men framför allt, mina bröder, svärjen icke, varken vid himmelen eller vid jorden, ej heller vid något annat, utan låten edert »ja» vara »ja», och edert »nej» vara »nej», så att I icke hemfallen under dom.
\x + Matt 5,33\x*
\p
\v 13 Får någon bland eder utstå lidande, så må han bedja.
\x + Ps 50,15; Ef 5,19\x*
\v 14 Är någon glad, så må han sjunga lovsånger. Är någon bland eder sjuk, må han då kalla till sig församlingens äldste; och dessa må bedja över honom och i Herrens namn smörja honom med olja.
\x + Mark 6,13.16,18\x*
\v 15 Och trons bön skall hjälpa den sjuke, och Herren skall låta honom stå upp igen; och om han har begått synder, skall detta bliva honom förlåtet.
\x + Ps 30,3\x*
\v 16 Bekännen alltså edra synder för varandra, och bedjen för varandra, på det att I mån bliva botade. Mycket förmår en rättfärdig mans bön, när den bedes med kraft.
\x + Ords 15,29\x*
\v 17 Elias var en människa, med samma natur som vi. Han bad en bön att det icke skulle regna, och det regnade icke på jorden under tre år och sex månader;
\x + 1 Kon 17,1; Sir 48,1; Luk 4,25\x*
\v 18 åter bad han, och då gav himmelen regn, och jorden bar sin frukt.
\x + 1 Kon 18,41\x*
\p
\v 19 Mina bröder, om någon bland eder har farit vilse från sanningen, och någon omvänder honom,
\x + Matt 18,15; Gal 6,1\x*
\v 20 så mån I veta att den som omvänder en syndare från hans villoväg, han frälsar hans själ från döden och överskyler en myckenhet av synder.
\x + Ords 10,12\x*
