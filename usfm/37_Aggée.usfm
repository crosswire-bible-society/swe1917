\id HAG
\h Haggai
\toc1 Haggai.
\toc2 Haggai.
\toc3 Hagg
\c 1
\s Folket bestraffas för sin försumlighet
\s vid tempelbygget.
\p
\v 1 I konung Darejaves' andra regeringsår, i sjätte månaden, på första dagen i månaden, kom HERRENS ord genom profeten Haggai Serubbabel, Sealtiels son, Juda ståthållare, och till översteprästen Josua, Josadaks son; han sade:
\x + Esr 5,1; Luk 3,27\x*
\p
\v 2 Så säger HERREN Sebaot: Detta folk säger: »Ännu är icke tiden kommen att gå till verket, tiden att HERRENS hus bygges upp.»
\v 3 Men HERRENS ord kom genom profeten Haggai; han sade:
\v 4 Är då tiden kommen för eder att själva bo i panelade hus, medan detta hus ligger öde?
\x + 2 Sam 7,2\x*
\p
\v 5 Därför säger nu HERREN Sebaot så: Given akt på huru det går eder.
\v 6 I sån mycket, men inbärgen litet; I äten, men fån icke nog för att bliva mätta; I dricken, men fån icke nog för att bliva glada; I tagen på eder kläder, men haven icke nog för att bliva varma. Och den som får någon inkomst, han far den allenast för att lägga den i en söndrig pung.
\x + 3 Mos 26,26; 5 Mos 28,38; Mik 6,14,; Hagg 2,17\x*
\p
\v 7 Ja, så säger HERREN Sebaot: Given akt på huru det går eder.
\v 8 Men dragen nu upp till bergen, hämten trävirke och byggen upp mitt hus, så vill jag hava behag därtill och bevisa mig härlig, säger HERREN.
\v 9 I väntaden på mycket, men se, det blev litet, och när I förden det hem, då blåste jag på det. Varför gick det så? säger HERREN Sebaot. Jo, därför att mitt hus får ligga öde, under det att envar av eder hastar med sitt eget hus.
\v 10 Fördenskull har himmelen ovan eder förhållit eder sin dagg och jorden förhållit sin gröda.
\x + 3 Mos 26,19; 5 Mos 28,23; Sak 8,12\x*
\v 11 Och jag har bjudit torka komma över land och berg, och över säd, vin och olja och alla andra jordens alster, och över människor och djur, och över all frukt av edra händers arbete.
\x + 1 Kon 17,1; 2 Kon 8,1; Hagg 2,18\x*
\p
\v 12 Och Serubbabel, Sealtiels son, och översteprästen Josua, Josadaks son med hela kvarlevan av folket lyssnade till HERRENS, sin Guds, röst och till profeten Haggais ord, eftersom HERREN, deras Gud, hade sänt honom; och folket fruktade för HERREN.
\v 13 Då sade Haggai, HERRENS sändebud, efter HERRENS uppdrag, till folket så: »Jag är med eder, säger HERREN.»
\x + Hagg 2,5\x*
\p
\v 14 Och HERREN uppväckte Serubbabels, Sealtiels sons, Juda ståthållares, ande och översteprästen Josuas, Josadaks sons, ande och allt det kvarblivna folkets ande, så att de gingo till verket och arbetade på HERREN Sebaots; sin Guds, hus.
\c 2
\p
\v 1 Detta skedde på tjugufjärde dagen i sjätte månaden av konung Darejaves' andra regeringsår.\f + \fr Hagg 2,1 \ft hör egentligen hit, men återfinns nedan. --Red för den elektroniska utgåvan.\f*
\s Det nya templet tillkommande härlighet.
\s En bättre tid. Serubbabels utkorelse.
\p
\v 2 I sjunde månaden, på tjuguförsta dagen i månaden, kom HERRENS ord genom profeten Haggai han sade:
\v 3 Säg till Serubbabel, Sealtiels son Juda ståthållare, och till översteprästen Josua, Josadaks son, och till kvarlevan av folket:
\p
\v 4 Leva icke ännu bland eder män kvar, som hava sett detta hus i dess forna härlighet? Och hurudant sen I det nu vara? Är det icke såsom intet i edra ögon?
\x + Esr 3,12\x*
\v 5 Men var likväl nu frimodig, du Serubbabel, säger HERREN; och var frimodig, du överstepräst Josua, Josadaks son; och varen frimodiga och arbeten, alla I som hören till folket i landet, säger HERREN; ty jag är med eder, säger HERREN Sebaot.
\x + Hagg 1,13\x*
\v 6 Det förbund som jag slöt med eder, när I drogen ut ur Egypten, vill jag låta stå fast, och min Ande skall förbliva ibland eder; frukten icke.
\x + 2 Mos 19,5; Sak 4,6\x*
\p
\v 7 Ty så säger HERREN Sebaot: Ännu en gång, inom en liten tid, skall jag komma himmelen och jorden, havet och det torra att bäva;
\x + Ps 60,4; Jes 14,16; Hebr 12,26\x*
\v 8 och alla hednafolk skall jag komma att bäva, och så skola dyrbara håvor från alla hednafolk föras hit; och jag skall fylla detta hus med härlighet, säger HERREN Sebaot.
\x + Jes 60,5.11\x*
\v 9 Ty mitt är silvret, och mitt är guldet, säger HERREN Sebaot.
\v 10 Den tillkommande härligheten hos detta hus skall bliva större än dess forna var, säger HERREN Sebaot; och på denna plats skall jag låta friden råda, säger HERREN Sebaot.
\x + Hagg 1,8\x*
\p
\v 11 På tjugufjärde dagen i nionde månaden av Darejaves' andra regeringsår kom HERRENS ord till profeten Haggai; han sade:
\v 12 Så säger HERREN Sebaot: Fråga prästerna om lag och säg:
\v 13 »Om någon bär heligt kött i fliken av sin mantel och så med fliken kommer vid något bakat eller kokt, eller vid vin eller olja, eller vid något annat som man förtär, månne detta därigenom bliver heligt?» Prästerna svarade och sade: »Nej.»
\v 14 Åter frågade Haggai: »Om den som har blivit orenad genom en död kommer vid något av allt detta, månne det då bliver orenat?» Prästerna svarade och sade: »Ja.»
\x + 1 Mos 19,11.16.22\x*
\p
\v 15 Då tog Haggai till orda och sade: »Så är det med detta folk och så är det med detta släkte inför mig, säger HERREN, och så är det med allt deras händers verk: vad de där offra, det är orent.
\v 16 Och given nu akt på huru det hittills har varit, före denna dag, och under tiden innan man ännu hade begynt lägga sten på sten till HERRENS tempel
\v 17 huru härförinnan, om någon kom till en sädesskyl som skulle giva tjugu mått, den gav allenast tio, och huru, om någon kom till vinpressen för att ösa upp femtio kärl, den gav allenast tjugu.
\v 18 Vid allt edra händers arbete slog jag eder säd med sot och rost och hagel, och likväl vänden I eder icke till mig, säger HERREN.
\x + 5 Mos 28,22; Am 4,9; Hagg 1,11\x*
\p
\v 19 Given alltså akt på huru det hittills har varit, före denna dag; ja, given akt på huru det har varit före tjugufjärde dagen i nionde månaden, denna dag då grunden har blivit lagd till HERRENS tempel.
\x + Esr 3,10\x*
\v 20 Finnes någon säd ännu i kornboden? Nej; och varken vinträdet eller fikonträdet eller granatträdet eller olivträdet har ännu burit någon frukt. Men från denna dag skall jag giva välsignelse.»
\v 21 Och HERRENS ord kom för andra gången till Haggai, på tjugufjärde dagen i samma månad; han sade:
\v 22 Säg till Serubbabel, Juda ståthållare: Jag skall komma himmelen och jorden att bäva;
\v 23 jag skall omstörta konungatroner och göra hednarikenas makt till intet; jag skall omstörta vagnarna med sina kämpar, och hästarna skola stupa med sina ryttare. Den ene skall falla för den andres svärd.
\v 24 På den tiden, säger HERREN Sebaot, skall jag taga dig, min tjänare Serubbabel, Sealtiels son, säger HERREN, och skall akta dig såsom min signetring; ty dig har jag utvalt, säger HERREN Sebaot.
\x + Jer 22,24; Sir 49,11\x*
