\id SNG
\h Höga Visan
\toc1 Höga Visan.
\toc2 Höga Visan.
\toc3 Höga
\c 1
\s Bruden talar om och till brudgummen v.
\p
\s 2--4. -- Bruden talar om sig själv v.
\s 5--6. -- Bruden stämmer möte med
\s brudgummen v. 7--8. -- Brudgummen talar
\s till bruden v. 9--11, hon svarar v.
\s 12--14, han talar v. 15, hon svarar åter
\s v. 16--17.
\p
\v 1 Sångernas sång av Salomo.
\p
\v 2 Kyssar give han mig, kyssar av sin mun! Ty din kärlek är mer ljuv än vin.
\v 3 Ljuv är doften av dina salvor, ja, en utgjuten salva är ditt namn; fördenskull hava tärnorna dig kär.
\p
\v 4 Drag mig med dig! Med hast vilja vi följa dig. Ja, konungen har fört mig in i sina gemak; Vi vilja fröjdas och vara glada över dig, vi vilja prisa din kärlek högre än vin; med rätta har man dig kär.
\s ----
\p
\v 5 Svart är jag, dock är jag täck, I Jerusalems döttrar, lik Kedars hyddor, lik Salomos tält.
\v 6 Sen icke därpå att jag är så svart, att solen har bränt mig så. Min moders söner blevo vreda på mig och satte mig till vingårdsvakterska; min egen vingård kunde jag icke vakta.
\v 7 »Säg mig, du som min själ har kär: Var för du din hjord i bet? Var låter du den vila om middagen? Må jag slippa att gå lik en vilsekommen kvinna vid dina vänners hjordar.»
\p
\v 8 »Om du icke vet det, du skönaste bland kvinnor, så gå blott åstad i hjordens spår, och för dina killingar i bet vid herdarnas tält.»
\s ----
\p
\v 9 »Vid ett sto i Faraos spann förliknar jag dig, min älskade.
\v 10 Dina kinder äro så täcka med sina kedjehängen, din hals med sina pärlerader.
\v 11 Kedjehängen av guld vilja vi skaffa åt dig med silverkulor på.»
\p
\v 12 »Medan konungen håller sin fest, sprider min nardus sin doft.
\v 13 Min vän är för mig ett myrragömme, som jag bär i min barm.
\v 14 Min vän är för mig en klase cyperblommor från En-Gedis vingårdar.»
\p
\v 15 »Vad du är skön, min älskade! Vad du är skön! Dina ögon äro duvor.»
\p
\v 16 »Vad du är skön, min vän! Ja, ljuvlig är du, och grönskande är vårt viloläger.
\v 17 Bjälkarna i vår boning äro cedrar, och cypresser vår väggpanel.»
\c 2
\s Bruden talar v. 1, brudgummen svarar v.
\p
\s 2, bruden talar åter v. 3--5. -- Bruden
\s talar till Jerusalems döttrar v. 6--7.
\s -- Bruden talar v. 8--9, anför
\s brudgummens ord v. 10--14. -- Vers om
\s vingårdens fiender v. 15. -- Bruden
\s talar v. 16--17.
\p
\v 1 »Jag är ett ringa blomster i Saron, en lilja i dalen.»
\p
\v 2 »Ja, såsom en lilja bland törnen, så är min älskade bland jungfrur.»
\v 3 »Såsom ett äppelträd bland vildmarkens träd, så är min vän bland ynglingar; ljuvligt är mig att sitta i dess skugga, och söt är dess frukt för min mun.
\p
\v 4 I vinsalen har han fört mig in, och kärleken är hans baner över mig.
\p
\v 5 Vederkvicken mig med druvkakor, styrken mig med äpplen; ty jag är sjuk av kärlek.»
\s ----
\p
\v 6 Hans vänstra arm vilar under mitt huvud, och hans högra omfamnar mig.
\p
\v 7 Jag besvär eder, I Jerusalems döttrar, vid gaseller och hindar på marken: Oroen icke kärleken, stören den icke, förrän den själv så vill.
\s ----
\p
\v 8 Hör, där är min vän! Ja, där kommer han, springande över bergen, hoppande fram på höjderna.
\v 9 Lik en gasell är min vän eller lik en ung hjort.
\p Se, nu står han där bakom vår vägg, han blickar in genom fönstret, han skådar genom gallret.
\v 10 Min vän begynner tala, han säger till mig:
\p »Stå upp, min älskade, du min sköna, och kom hitut.
\v 11 Ty se, vintern är förbi, regntiden är förliden och har gått sin kos.
\v 12 Blommorna visa sig på marken, tiden har kommit, då vinträden skäras, och turturduvan låter höra sin röst i vårt land.
\v 13 Fikonträdets frukter begynna att mogna, vinträden stå redan i blom, de sprida sin doft. Stå upp, min älskade, min sköna, och kom hitut.
\p
\v 14 Du min duva i bergsklyftan, i klippväggens gömsle, låt mig se ditt ansikte, låt mig höra din röst; ty din röst är så ljuv, och ditt ansikte är så täckt.»
\s ----
\p
\v 15 Fången rävarna åt oss, de små rävarna, vingårdarnas fördärvare, nu då våra vingårdar stå i blom.
\s ----
\p
\v 16 Min vän är min, och jag är hans, där han för sin hjord i bet ibland liljor.
\v 17 Till dess morgonvinden blåser och skuggorna fly, må du ströva omkring, lik en gasell, min vän, eller lik en ung hjort, på de kassiadoftande bergen.
\c 3
\s Bruden berättar en dröm v. 1--4. --
\p
\s Bruden talar till Jerusalems döttrar v.
\s 5. -- Brudtåget beskrives v. 6--11.
\p
\v 1 Där jag låg på mitt läger om natten, sökte jag honom som min själ har kär; jag sökte honom, men fann honom icke.
\p
\v 2 »Jag vill stå upp och gå omkring i staden, på gatorna och på torgen; jag vill söka honom som min själ har kär.»
\p Jag sökte honom, men fann honom icke.
\p
\v 3 Väktarna mötte mig, där de gingo omkring i staden. »Haven I sett honom som min själ har kär?»
\p
\v 4 Knappt hade jag kommit förbi dem, så fann jag honom som min själ har kär. Jag tog honom fatt, och jag släppte honom icke, förrän jag hade fört honom in i min moders hus, in i min fostrarinnas kammare.
\s ----
\p
\v 5 Jag besvär eder, I Jerusalems döttrar, vid gaseller och hindar på marken: Oroen icke kärleken, stören den icke, förrän den själv så vill.
\s ----
\p
\v 6 Vem är hon som kommer hitupp från öknen såsom i stoder av rök, kringdoftad av myrra och rökelse och alla slags köpmannakryddor?
\p
\v 7 Se, det är Salomos bärstol! Sextio hjältar omgiva den, utvalda bland Israels hjältar.
\v 8 Alla bära de svärd och äro väl förfarna i strid. Var och en har sitt svärd vid sin länd, till värn mot nattens faror.
\p
\v 9 En praktbår är det som konung Salomo har låtit göra åt sig av virke från Libanon.
\v 10 Dess sidostöd äro gjorda av silver, ryggstödet av guld, sätet belagt med purpurrött tyg. Innantill är den prydd i kärlek av Jerusalems döttrar.
\v 11 I Sions döttrar, gån ut och skåden konung Salomo med lust, skåden kransen som hans moder har krönt honom med på hans bröllopsdag, på hans hjärtefröjds dag.
\c 4
\s Brudgummen beskriver bruden v. 1--7. --
\p
\s Brudgummen vill hämta bruden från bergen
\s v. 8. -- Brudgummen talar till bruden v.
\s 9--11. -- Brudgummen talar v. 12--15,
\s bruden svarar v. 16, brudgummen svarar
\s åter 5,1a. -- Lyckönskan till brud och
\s brudgum 5,1b.
\p
\v 1 Vad du är skön, min älskade, vad du är skön! Dina ögon äro duvor, där de skymta genom din slöja. Ditt hår är likt en hjord av getter som strömma nedför Gileads berg.
\v 2 Dina tänder likna en hjord av nyklippta tackor, nyss uppkomna ur badet, allasammans med tvillingar, ofruktsam är ingen ibland dem.
\v 3 Ett rosenrött snöre likna dina läppar, och täck är din mun. Lik ett brustet granatäpple är din kind, där den skymtar genom din slöja.
\v 4 Din hals är lik Davids torn, det väl befästa; tusen sköldar hänga därpå, hjältarnas alla sköldar.
\v 5 Din barm är lik ett killingpar, tvillingar av en gasell, som gå i bet ibland liljor.
\p
\v 6 Till dess morgonvinden blåser och skuggorna fly, vill jag gå bort till myrraberget, till den rökelsedoftande höjden.
\p
\v 7 Du är skön alltigenom, min älskade, på dig finnes ingen fläck.
\s ----
\p
\v 8 Kom med mig från Libanon, min brud, kom med mig från Libanon. Stig ned från Amanas topp, från toppen av Senir och Hermon, från lejonens hemvist, från pantrarnas berg.
\s ----
\p
\v 9 Du har tagit mitt hjärta, du min syster, min brud; du har tagit mitt hjärta med en enda blick, med en enda länk av kedjan kring din hals.
\p
\v 10 Huru skön är icke din kärlek, du min syster, min brud! Huru ljuv är icke din kärlek! Ja, mer ljuv än vin; och doften av dina salvor övergår all vällukt.
\v 11 Av sötma drypa dina läppar, min brud; din tunga gömmer honung och mjölk, och doften av dina kläder är såsom Libanons doft.
\s ----
\p
\v 12 »En tillsluten lustgård är min syster, min brud, en tillsluten brunn, en förseglad källa.
\v 13 Såsom en park av granatträd skjuter du upp, med de ädlaste frukter, med cyperblommor och nardusplantor,
\v 14 med nardus och saffran, kalmus och kanel och rökelseträd av alla slag, med myrra och aloe och de yppersta kryddor av alla slag.
\p
\v 15 Ja, en källa i lustgården är du, en brunn med friskt vatten och ett rinnande flöde ifrån Libanon.»
\p
\v 16 »Vakna upp, du nordanvind, och kom, du sunnanvind; blås genom min lustgård, låt dess vällukt strömma ut. Må min vän komma till sin lustgård och äta dess ädla frukter.»
\p
\c 5
\p
\v 1 »Ja, jag kommer till min lustgård, du min syster, min brud; jag hämtar min myrra och mina välluktande kryddor, jag äter min honungskaka och min honung, jag dricker mitt vin och min mjölk.»\f + \fr Höga V. 5,1 \ft hör egentligen hit, men återfinns nedan. --Red för den elektroniska utgåvan.\f*
\s ----
\p Äten, I kära, och dricken, ja, berusen eder av kärlek.
\p
\p
\s Bruden talar v. 2a, anför brudgummens
\s ord v. 2b och sitt svar v. 3, talar
\s vidare v. 4--8; Jerusalems döttrar tala
\s v. 9, bruden svarar och beskriver sin
\s brudgum för Jerusalems döttrar v.
\s 10--16, Jerusalems döttrar fråga v. 17,
\s bruden svarar 6,1--2.
\p
\v 2 Jag låg och sov, dock vakade mitt hjärta. Hör, då klappar min vän på dörren:
\p »Öppna för mig, du min syster, min älskade, min duva, min fromma; ty mitt huvud är fullt av dagg, mina lockar av nattens droppar.»
\p
\v 3 »Jag har lagt av mina kläder; skulle jag nu åter taga dem på mig? Jag har tvagit mina fötter; skulle jag nu orena dem?»
\p
\v 4 Min vän räckte in sin hand genom luckan; då rördes mitt hjärta över honom.
\v 5 Jag stod upp för att öppna för min vän, och mina händer dröpo av myrra, mina fingrar av flytande myrra, som fuktade rigelns handtag.
\p
\v 6 Så öppnade jag för min vän, men min vän var borta och försvunnen. Min själ blev utom sig vid tanken på hans ord.
\p Jag sökte honom, men fann honom icke; jag ropade på honom, men han svarade mig icke.
\v 7 Väktarna mötte mig, där de gingo omkring i staden, de slogo mig, de sårade mig; de ryckte av mig min mantel, väktarna på murarna.
\p
\v 8 »Jag besvär eder, I Jerusalems döttrar, om I finnen min vän, så sägen -- ja, vad skolen I säga honom? Att jag är sjuk av kärlek!»
\p
\v 9 »Vad är då din vän förmer än andra vänner, du skönaste bland kvinnor? Vad är din vän förmer än andra vänner, eftersom du så besvär oss?»
\p
\v 10 »Min vän är strålande vit och röd, härlig framför tio tusen.
\v 11 Hans huvud är finaste guld, hans lockar palmträdsvippor, och svarta såsom korpen.
\v 12 Hans ögon likna duvor invid vattenbäckar, duvor som bada sig i mjölk och sitta invid bräddfull rand.
\v 13 Hans kinder liknar välluktrika blomstersängar, skrin med doftande kryddor. Hans läppar äro röda liljor; de drypa av flytande myrra.
\v 14 Hans händer äro tenar av guld, besatta med krysoliter. Hans midja är formad av elfenben, övertäckt med safirer.
\v 15 Hans ben äro pelare av vitaste marmor, som vila på fotstycken av finaste guld. Att se honom är såsom att se Libanon; ståtlig är han såsom en ceder.
\v 16 Hans mun är idel sötma, hela hans väsende är ljuvlighet. Sådan är min vän, ja, sådan är min älskade, I Jerusalems döttrar.»
\p
\v 17 »Vart har han då gått, din vän, du skönaste bland kvinnor? Vart har din vän tagit vägen? Låt oss hjälpa dig att söka honom.»
\c 6
\p
\v 1 »Min vän har gått ned till sin lustgård, till sina välluktrika blomstersängar, för att låta sin hjord beta i lustgårdarna och för att plocka liljor.\f + \fr Höga V. 6,1--2 \ft hör egentligen hit, men återfinns nedan. --Red för den elektroniska utgåvan.\f*
\p
\v 2 Jag är min väns, och min vän är min, där han för sin hjord i bet ibland liljor.
\p
\s Brudgummen beskriver bruden v. 3--6. --
\s Brudgummen jämför sin brud med andra v.
\s 7--8. -- Brudgummen talar om bruden och
\s sig själv v. 9--11.
\p
\v 3 Du är skön såsom Tirsa, min älskade, ljuvlig såsom Jerusalem, överväldigande såsom en härskara.
\p
\v 4 Vänd bort ifrån mig dina ögon, ty de hava underkuvat mig. Ditt hår är likt en hjord av getter som strömma nedför Gilead.
\v 5 Dina tänder likna en hjord av tackor, nyss uppkomna ur badet, allasammans med tvillingar, ofruktsam är ingen ibland dem.
\v 6 Lik ett brustet granatäpple är din kind, där den skymtar genom din slöja.
\s ----
\p
\v 7 Sextio äro drottningarna, och åttio bihustrurna, och tärnorna en otalig skara.
\v 8 Men en enda är hon, min duva, min fromma, hon, sin moders endaste, hon, sin fostrarinnas utkorade. När jungfrur se henne, prisa de henne säll, drottningar och bihustrur höja hennes lov.
\s ----
\p
\v 9 Vem är hon som där blickar fram lik en morgonrodnad, skön såsom månen, strålande såsom solen, överväldigande såsom en härskara?
\p
\v 10 Till valnötslunden gick jag ned, för att glädja mig åt grönskan i dalen, för att se om vinträden hade slagit ut, om granatträden hade fått blommor.
\v 11 Oförtänkt satte mig då min kärlek upp på mitt furstefolks vagnar.
\p
\s Den dansande bruden beskrives 6,12--7,5.
\s -- Brudgummen talar om sin kärlek och om
\s bruden v. 6--9a, bruden svarar v.
\s 9b--10. -- Bruden talar v. 11--13.
\p
\v 12 »Vänd om, vänd om, du brud från Sulem, vänd om, vänd om, så att vi få se på dig.» »Vad finnen I att se hos bruden från Sulem, där hon rör sig såsom i vapendans?»
\c 7
\p
\v 1 »Huru sköna äro icke dina fötter i sina skor, du ädla! Dina höfters rundning är såsom ett bröstspännes kupor, gjorda av en konstnärs händer.
\v 2 Ditt sköte är en rundad skål, må vinet aldrig fattas däri. Din midja är en vetehög, omhägnad av liljor.
\v 3 Din barm är lik ett killingpar, tvillingar av en gasell.
\v 4 Din hals liknar Elfenbenstornet, dina ögon dammarna i Hesbon, vid Bat-Rabbimsporten. Din näsa är såsom Libanonstornet, som skådar ut mot Damaskus.
\v 5 Ditt huvud höjer sig såsom Karmel, och lockarna på ditt huvud hava purpurglans. En konung är fångad i deras snara.»
\s ----
\p
\v 6 »Huru skön och huru ljuv är du icke, du kärlek, så följd av lust!
\p
\v 7 Ja, din växt är såsom ett palmträds, och din barm liknar fruktklasar.
\v 8 Jag tänker: I det palmträdet vill jag stiga upp, jag vill gripa tag i dess kvistar. Må din barm då vara mig såsom vinträdets klasar och doften av din andedräkt såsom äpplens doft
\v 9 och din mun såsom ljuvaste vin!»
\p »Ja, ett vin som lätt glider ned i min vän och fuktar de slumrandes läppar.
\v 10 Jag är min väns, och till mig står hans åtrå.»
\s ----
\p
\v 11 Kom, min vän; låt oss gå ut på landsbygden och stanna i byarna över natten.
\v 12 Bittida må vi gå till vingårdarna, för att se om vinträden hava slagit ut, om knopparna hava öppnat sig, om granatträden hava fått blommor. Där vill jag giva min kärlek åt dig.
\p
\v 13 Kärleksäpplena sprida sin doft, och vid våra dörrar finnas alla slags ädla frukter, både nya och gamla; åt dig, min vän, har jag förvarat dem.
\c 8
\s Bruden talar v. 1--2. -- Bruden talar
\p
\s till Jerusalems döttrar v. 3--4. --
\s Brudens uppträdande omtalas v. 5a.
\s Bruden talar 5b--7. -- En av brudens
\s bröder talar v. 8, en annan broder v. 9,
\s bruden själv v. 10. -- Brudgummen talar
\s v. 11--12. -- Brudgummen talar v. 13,
\s bruden v. 14.
\p
\v 1 Ack att du vore såsom en min broder, ammad vid min moders bröst! Om jag då mötte dig därute, så finge jag kyssa dig, och ingen skulle tänka illa om mig därför.
\v 2 Jag finge då ledsaga dig, föra dig in i min moders hus, och du skulle undervisa mig; kryddat vin skulle jag giva dig att dricka, saft från mitt granatträd.
\s ----
\p
\v 3 Hans vänstra arm vilar under mitt huvud, och hans högra omfamnar mig.
\p
\v 4 Jag besvär eder, I Jerusalems döttrar: Oroen icke kärleken, stören den icke, förrän den själv så vill.
\p
\v 5 Vem är hon som kommer hitupp från öknen, stödd på sin vän?
\p »Där under äppelträdet väckte jag dig; där var det som din moder hade fött dig, där födde dig hon som gav dig livet.
\p
\v 6 Hav mig såsom en signetring vid ditt hjärta, såsom en signetring på din arm.
\p Ty kärleken är stark såsom döden, dess trängtan obetvinglig såsom dödsriket; dess glöd är såsom eldens glöd, en HERRENS låga är den.
\v 7 De största vatten förmå ej utsläcka kärleken, strömmar kunna icke fördränka den. Om någon ville giva alla ägodelar i sitt hus för kärleken, så skulle han ändå bliva försmådd.»
\s ----
\p
\v 8 »Vi hava en syster, en helt ung, som ännu icke har någon barm. Vad skola vi göra med vår syster, när tiden kommer, att man vill vinna henne?»
\v 9 »Är hon en mur, så bygga vi på den ett krön av silver; men är hon en dörr, så bomma vi för den med en cederplanka.»
\p
\v 10 »Jag är en mur, och min barm är såsom tornen därpå; så blev jag i hans ögon en kvinna som var ynnest värd.»
\s ----
\p
\v 11 En vingård ägde Salomo i Baal-Hamon, den vingården lämnade han åt väktare; tusen siklar silver var kunde de hämta ur dess frukt.
\p
\v 12 Men min vingård, den har jag själv i min vård. Du, Salomo, må taga dina tusen, och två hundra må de få, som vakta dess frukt.
\s ----
\p
\v 13 »Du lustgårdarnas inbyggerska, vännerna lyssna efter din röst; låt mig höra den.»
\p
\v 14 »Skynda åstad, min vän, lik en gasell eller lik en ung hjort, upp på de välluktrika bergen.»
